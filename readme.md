This repository has four parts.
* `Pencil-fullhe`: Pencil framework without the preprocessing optimization.
* `Pencil-prep`: Pencil framework which the preprocessing optimization.
* `seal-cuda`: A GPU implementation of BFV/BGV/CKKS scheme, based on SEAL. [Original SEAL](https://github.com/Microsoft/SEAL)
* `OpenCheetah`: An edited version of OpenCheetah library, including support for bit injection and python encapsulation. [Original OpenCheetah](https://github.com/Alibaba-Gemini-Lab/OpenCheetah)

Pencil require the latter two as dependency modules.